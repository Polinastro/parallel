program openmp
 use Task
 use :: mpi
 implicit none
 real(8), allocatable, dimension(:,:) :: A
 integer(4) :: n, m, x1, x2, y1, y2,mpiErr
 double precision starttime, endtime
 
 open(1, file = "input") 
 open(2, file = "output")
 read(1,*) n, m
 allocate (A(n,m))
 call random_number(A)
call mpi_init(mpiErr)

 starttime = omp_get_wtime()

 call GetMaxCoordinates(A, x1, y1, x2, y2)

 endtime = omp_get_wtime() 
 write(*,*) "time= ", endtime - starttime
call mpi_finalize(mpiErr)
 write(2,*) x1, y1, x2, y2
 close(1); close(2)
end program
